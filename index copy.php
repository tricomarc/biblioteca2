<?php 
require_once "conf/Config.php";
require_once ROOT_PATH ."/dao/UsuarioDAO.php";
require_once ROOT_PATH ."/model/Usuario.php";
require_once ROOT_PATH ."/controller/SessionController.php";


$controller = new SessionController();

if(!$controller->estaAutenticado()) {
    header("Location: login.php");
    die();
}


// Cereación de usuario
if($_SERVER['REQUEST_METHOD'] == 'POST'){

    // Rescata los datos del formulario
    $usuario = new Usuario();

    $usuario-> setUsername($_POST["txtUsuario"]);
    $usuario->setNombre($_POST["txtNombre"]);
    $usuario->setApellido($_POST["txtApellido"]);
    $usuario->setFechaNacimiento($_POST["txtFecha"]);
    $usuario->setPassword($_POST["txtContrasena"]);

    // Imprime el usuario obtenido
   /* echo "Usuario obtenido: " . $usuario->getUsername() . " - " . $usuario->getNombre() . " - "
    . $usuario->getApellido() . " - " . $usuario->getFechaNacimiento() . " - " . $usuario->getPassword();
*/

 //   $insertar = new SessionController();

    $controller->insertarUsuario($usuario);

    if($controller){
      echo '</br>';
      echo 'Usuario Insertado';
  
    }else{
      echo '</br>';
      echo 'Usuario NO Insertado';
    }

  }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registro de Usuarios</title>
        
        <link rel="stylesheet" type="text/css" href="librerias/bootstrap4/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php require_once "Insertar.php" ?>

        <script src="librerias/bootstrap4/jquery-3.4.1.min.js"></script>
        <script src="librerias/bootstrap4/popper.min.js"></script>
        <script src="librerias/bootstrap4/bootstrap.min.js"></script>
        <script src="librerias/sweetalert.min.js"></script>
        <script src="js/crud.js"></script>


        <script type="text/javascript">
            mostrar();
        </script>
        
        <script>
        
            function modificar(usuario) {
                location.href='editar.php?username='+usuario;
            }
        
            function eliminar(usuario) {
                document.formularioEliminar.username.value=usuario;
                formularioEliminar.submit();
            }

        </script>
        
    </head>
    <body>
        <h1>Demo Admin Login</h1>

        [<a href="logout.php" >Logout <?= $controller->getNombreUsuarioAutenticado() ?></a>]
        
<?php 
    $usuarios = $controller->getUsuarios();
?>
        <table>
            <thead>
                <tr>
                    <th>USERNAME</th>
                    <th>NOMBRE</th>
                    <th>APELLIDO</th>
                    <th>FECHA_NACIMIENTO</th>
                    <th>MODIFICAR</th>
                    <th>ELIMINAR</th>
                </tr>
            </thead>
            <tbody>
                
                <?php
                    foreach($usuarios as $user) {
                        /* @var $user Usuario */
                ?>

                <tr>
                    <td><?= $user->getUsername() ?></td>
                    <td><?= $user->getNombre() ?></td>
                    <td><?= $user->getApellido() ?></td>
                    <td><?= $user->getFechaNacimiento() ?></td>
                    <td>
                        <input type="button" 
                               name="modificar" 
                               value="Modificar" 
                               onclick="javascript:modificar('<?= $user->getUsername() ?>');" />
                    </td>
                    <td>
                        <input type="button" 
                               name="eliminar" 
                               value="Eliminar" 
                               onclick="javascript:eliminar('<?= $user->getUsername() ?>');" />
                    </td>
                    <td>
                        <div class="row">
                            <div class="col-sm-12">
                                <span class="btn btn-primary" data-toggle="modal" data-target="#insertarModal">
                                    <i class="fas fa-plus-circle"></i> Nuevo registro
                                </span>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php
                    }
                ?>                
            </tbody>            
        </table>
        
        <form name="formularioEliminar" id="formularioEliminar" 
              action="eliminar.php" method="POST">
            <input type="hidden" name="username" value=""/>
        </form>
        
    </body>
</html>


