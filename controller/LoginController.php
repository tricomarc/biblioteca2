<?php

require_once ROOT_PATH ."/dao/UsuarioDAO.php";
require_once ROOT_PATH ."/dao/impl/UsuarioDaoSqlImpl.php";

class LoginController {
    
    /**
     *
     * @var UsuarioDAO 
     */
    private $daoUsuario;
    
    public function __construct() {
        $this->daoUsuario = new UsuarioDaoSqlImpl();
    }
    
    function getUsuarios() {
        $usuarios = $this->daoUsuario->listar();
        
        return $usuarios;        
    }
    
}
