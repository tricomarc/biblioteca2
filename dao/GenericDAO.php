<?php

interface GenericDAO {
   
    function agregar($objeto);
    
    function modificar($objeto);
    
    function eliminar($objeto);
       
    function listar();
}
