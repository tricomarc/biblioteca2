
<?php

  require_once "conf/Config.php";
  require_once ROOT_PATH ."/dao/UsuarioDAO.php";
  require_once ROOT_PATH ."/model/Usuario.php";
  require_once ROOT_PATH ."/controller/SessionController.php";

  
?>

<!-- Modal -->
<div class="modal fade" id="insertarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agregar nuevo usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="frminsert" action="index.php" method="POST">
              <label>Usuario</label>
              <input type="text" id="txtUsuario" name="txtUsuario" class="form-control form-control-sm" required="">
              <label>Nombre</label>
              <input type="text" id="txtNombre" name="txtNombre" class="form-control form-control-sm" required="">
              <label>Apellido</label>
              <input type="text" id="txtApellido" name="txtApellido" class="form-control form-control-sm">
              <label>Fecha de nacimiento</label>
              <input type="date" id="txtFecha" name="txtFecha" class="form-control form-control-sm">
              <label>Contraseña</label>
              <input type="text" id="txtContrasena" name="txtContrasena" class="form-control form-control-sm">
              <br>
               <input type="submit" value="Guardar"  id="btnGuardar" name="btnGuardar" class="btn btn-primary">
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>

